﻿using Core;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAOMock1.BO
{
    public class Car : ICar
    {
        public int ID { get; set; } = -1;
        public string Name { get; set; }
        public int ProductionYear { get; set; } = -1;
        public IProducer Producer { get; set; }
        public TransmissionType Transmission { get; set; }

        public override string ToString()
        {
            return $"Name: {Name} - {ProductionYear} [ {Producer.Name} ] {Transmission} ";
        }
    }
}
