﻿using Interfaces;
using System;
using System.Collections.Generic;

namespace DAOMock1
{
    public class DB : IDAO
    {
        private List<IProducer> _producers;
        private List<ICar> _cars;

        public DB()
        {
            _producers = new List<IProducer>()
            {
                new BO.Producent{ ID=1, Name="Opel", Founded=1940 },
                new BO.Producent{ ID=2, Name="Fiat", Founded=1950 },
                new BO.Producent{ ID=3, Name="Volvo", Founded=1930}
            };

            _cars = new List<ICar>()
            {
                new BO.Car
                {
                    ID =1,
                    Name ="Corsa",
                    Producer = _producers[0],
                    ProductionYear =1999,
                    Transmission =Core.TransmissionType.Automatic
                },
                new BO.Car
                {
                    ID =2,
                    Name ="Astra",
                    Producer = _producers[0],
                    ProductionYear =2009,
                    Transmission =Core.TransmissionType.Automatic
                },
                new BO.Car
                {
                    ID =3,
                    Name ="Panda",
                    Producer = _producers[1],
                    ProductionYear =2000,
                    Transmission =Core.TransmissionType.Manual
                },
                new BO.Car
                {
                    ID =4,
                    Name ="Multipla",
                    Producer = _producers[0],
                    ProductionYear = 2007,
                    Transmission =Core.TransmissionType.Manual
                },
                new BO.Car
                {
                    ID =5,
                    Name ="S40",
                    Producer = _producers[2],
                    ProductionYear =1994,
                    Transmission =Core.TransmissionType.Automatic
                },
                new BO.Car
                {
                    ID =6,
                    Name ="Mustang",
                    Producer = _producers[0],
                    ProductionYear =2004,
                    Transmission =Core.TransmissionType.Automatic
                },
                new BO.Car
                {
                    ID =7,
                    Name ="Pilot",
                    Producer = _producers[2],
                    ProductionYear =2010,
                    Transmission =Core.TransmissionType.Manual
                },
                new BO.Car
                {
                    ID =8,
                    Name ="FR-V",
                    Producer = _producers[1],
                    ProductionYear =2005,
                    Transmission =Core.TransmissionType.Manual
                }


            };
        }

        public ICar CreateEmptyCar()
        {
            return new BO.Car();
        }

        public IProducer CreateEmptyProducer()
        {
            return new BO.Producent();
        }

        public IProducer CloneProducer(IProducer producer)
        {
            try
            {
                return new BO.Producent
                {
                    ID = producer.ID,
                    Name = producer.Name,
                    Founded = producer.Founded
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<ICar> GetAllCars()
        {
            return _cars;
        }

        public IEnumerable<IProducer> GetAllProducers()
        {
            return _producers;
        }

        public void DeleteCar(ICar car)
        {
            _cars.Remove(car);
        }

        public void UpdateCar(ICar oldCar, ICar newCar)
        {
            if (_cars.Contains(oldCar))
            {
                _cars[_cars.IndexOf(oldCar)] = newCar;
            }
        }

        public void AddCar(ICar car)
        {
            _cars.Add(car);
        }

        public void AddProducer(IProducer producer)
        {
            _producers.Add(producer);
        }
    }
}
