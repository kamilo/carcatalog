﻿namespace Interfaces
{
    public interface ICar
    {
        int ID { get; set; }
        string Name { get; set; }
        int ProductionYear { get; set; }
        IProducer Producer { get; set; }
        Core.TransmissionType Transmission { get; set; }
    }
}
