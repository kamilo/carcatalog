﻿using System.Collections.Generic;

namespace Interfaces
{
    public interface IDAO
    {
        IEnumerable<IProducer> GetAllProducers();
        IEnumerable<ICar> GetAllCars();
        ICar CreateEmptyCar();
        IProducer CreateEmptyProducer();
        void DeleteCar(ICar car);
        void UpdateCar(ICar oldCar, ICar newCar);
        void AddCar(ICar car);
        void AddProducer(IProducer producer);
    }
}
