﻿using Core;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CarCatalog.ViewModel
{
    public class CarViewModel : ViewModelBase, ICar
    {
        private ICar _car;

        public CarViewModel(ICar car, ObservableCollection<IProducer> producers)
        {
            _car = car;
            _producers = producers;
            //Console.WriteLine("VM Init " + car.Producer.Name + ": " + car.Producer.GetHashCode());
        }

        [Required(ErrorMessage = "ID musi być ustalone.")]
        [Range(0, int.MaxValue, ErrorMessage = "ID musi być liczbą całkowitą")]
        public string CarId
        {
            get => (_car.ID == -1 ? "" : _car.ID.ToString());
            set
            {
                try
                {
                    _car.ID = int.Parse(value);
                }
                catch (Exception)
                {
                    if (value == "")
                    {
                        _car.ID = -1;
                    }
                }
                OnPropertyChanged("CarId");
                Validate();
            }
        }

        [Required(ErrorMessage = "Nazwa musi być ustalona")]
        [StringLength(15, MinimumLength = 3, ErrorMessage = "Długość nazwy musi być z przedziału od 3 do 15")]
        public string Name
        {
            get => _car.Name;
            set
            {
                _car.Name = value;
                Validate();
                OnPropertyChanged("Name");
            }
        }

        [Required(ErrorMessage = "Rok produkcji jest wymagany")]
        [Range(1900, 2020, ErrorMessage = "Rok produkcji musi być z przedziału od 1900 do 2020")]
        public string ProdYear
        {
            get => (_car.ProductionYear == -1 ? "" : _car.ProductionYear.ToString());
            set
            {
                try
                {
                    _car.ProductionYear = int.Parse(value);
                }
                catch (Exception)
                {
                    if (value == "")
                    {
                        _car.ProductionYear = -1;
                    }
                }
                Validate();
                OnPropertyChanged("ProdYear");
            }
        }

        [Required(ErrorMessage = "Producent musi być wybrany")]
        public IProducer Producer
        {
            get => _car.Producer;
            set
            {
                _car.Producer = value;
                Validate();
                OnPropertyChanged("Producer");
            }
        }

        private ObservableCollection<IProducer> _producers;

        public ObservableCollection<IProducer> Producersx
        {
            get => _producers;
        }

        public int ID { get => _car.ID; set => _car.ID = value; }
        public int ProductionYear { get => _car.ProductionYear; set => _car.ProductionYear = value; }

        [Required(ErrorMessage = "Skrzynia biegów musi być wybrana")]
        public TransmissionType Transmission
        {
            get => _car.Transmission;
            set
            {
                _car.Transmission = value;
                Validate();
                OnPropertyChanged("Transmission");
            }
        }

        public IList<TransmissionType> TransmissionTypes
        {
            get
            {
                return Enum.GetValues(typeof(TransmissionType)).Cast<TransmissionType>().ToList<TransmissionType>();
            }
        }

    }
}
