﻿using Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace CarCatalog.ViewModel
{
    public class ProducerViewModel : ViewModelBase, IProducer
    {
        private IProducer _producer;

        public ProducerViewModel(IProducer producer)
        {
            _producer = producer;
        }

        [Required(ErrorMessage = "ID musi być ustalone.")]
        [Range(0, int.MaxValue, ErrorMessage = "ID musi być liczbą całkowitą")]
        public string ProducerId
        {
            get => (_producer.ID == -1 ? "" : _producer.ID.ToString());
            set
            {
                try
                {
                    _producer.ID = int.Parse(value);
                }
                catch (Exception)
                {
                    if (value == "")
                    {
                        _producer.ID = -1;
                    }
                }
                OnPropertyChanged("ProducerId");
                Validate();
            }
        }

        [Required(ErrorMessage = "Nazwa musi być ustalona")]
        [StringLength(15, MinimumLength = 3, ErrorMessage = "Długość nazwy musi być z przedziału od 3 do 15")]
        public string Name
        {
            get => _producer.Name;
            set
            {
                _producer.Name = value;
                Validate();
                OnPropertyChanged("ProducerName");
            }
        }

        [Required(ErrorMessage = "Rok założenia jest wymagany")]
        [Range(1800, 2020, ErrorMessage = "Rok produkcji musi być z przedziału od 1800 do 2020")]
        public string FoundedYear
        {
            get => (_producer.Founded == -1 ? "" : _producer.Founded.ToString());
            set
            {
                try
                {
                    _producer.Founded = int.Parse(value);
                }
                catch (Exception)
                {
                    if (value == "")
                    {
                        _producer.Founded = -1;
                    }
                }
                Validate();
                OnPropertyChanged("FoundedYear");
            }
        }

        public int ID { get => _producer.ID; set => _producer.ID = value; }
        public int Founded { get => _producer.Founded; set => _producer.Founded = value; }

    }
}
