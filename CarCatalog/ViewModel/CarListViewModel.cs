﻿using Interfaces;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Data;

namespace CarCatalog.ViewModel
{
    public class CarListViewModel : ViewModelBase
    {
        private ListCollectionView _view;
        private BLC.BLC blc;

        private ObservableCollection<ICar> _cars;
        public ObservableCollection<ICar> Cars
        {
            get => _cars;
            set
            {
                _cars = value;
                OnPropertyChanged(nameof(Cars));
            }
        }

        private ObservableCollection<IProducer> _producers;
        public ObservableCollection<IProducer> Producers
        {
            get => _producers;
            set
            {
                _producers = value;
                OnPropertyChanged(nameof(Producers));
            }
        }

        private string _filterValue;
        public string FilterValue
        {
            get { return _filterValue; }
            set
            {
                _filterValue = value;
                FilterData();
                OnPropertyChanged(nameof(FilterValue));
            }
        }

        public CarListViewModel()
        {
            Properties.Settings sett = new Properties.Settings();
            blc = new BLC.BLC(sett.DBNameConf);
            _cars = new ObservableCollection<ICar>();
            _producers = new ObservableCollection<IProducer>(blc.GetProducers());

            OnPropertyChanged("Cars");
            GetAllCars();
            _view = (ListCollectionView)CollectionViewSource.GetDefaultView(Cars);
            _addNewCarCommand = new RelayCommand(param => this.AddNewCar(),
                                                 param => this.CanAddNewCar());
            _deleteCarCommand = new RelayCommand(param => this.DeleteCar(),
                                                 param => this.CanDeleteCar());
            _saveCarCommand = new RelayCommand(param => this.SaveCar(),
                                                param => this.CanSaveCar());
            _addNewProducerCommand = new RelayCommand(param => this.AddNewProducer(),
                                                 param => this.CanAddNewProducer());
            _saveProducerCommand = new RelayCommand(param => this.SaveProducer(),
                                    param => this.CanSaveProducer());
            _cancelProducerCommand = new RelayCommand(param => this.CancelProducer(),
                                    param => this.CanCancelProducer());
        }

        private void GetAllCars()
        {
            foreach (var c in blc.GetCars())
            {
                _cars.Add(new CarViewModel(c, _producers));
            }
        }

        private void FilterData()
        {
            if (string.IsNullOrEmpty(FilterValue))
            {
                _view.Filter = null;
            }
            else
            {
                _view.Filter = (c) => ((CarViewModel)c).Name.ToLower().Contains(FilterValue.ToLower());
            }
        }

        private CarViewModel _formCar;
        public CarViewModel FormCar
        {
            get => _formCar;
            set
            {
                _formCar = value;
                OnPropertyChanged(nameof(FormCar));
                OnPropertyChanged(nameof(FormEnabled));
            }
        }

        private CarViewModel _editedCar;
        public CarViewModel EditedCar
        {
            get => _editedCar;
            set
            {
                _editedCar = value;
                if (value != null)
                {
                    FormCar = new CarViewModel(blc.CreateEmptyCar(), _producers);
                    FormCar.Name = value.Name;
                    FormCar.CarId = value.CarId;
                    FormCar.Producer = value.Producer;
                    FormCar.ProdYear = value.ProdYear;
                    FormCar.Transmission = value.Transmission;
                }
                else
                {
                    FormCar = null;
                }
                OnPropertyChanged(nameof(EditedCar));
            }
        }

        private void AddNewCar()
        {
            EditedCar = new CarViewModel(blc.CreateEmptyCar(), _producers);
            EditedCar.Validate();
        }

        private RelayCommand _addNewCarCommand;

        public RelayCommand AddNewCarCommand
        {
            get => _addNewCarCommand;
        }

        private bool CanAddNewCar()
        {
            if (EditedCar != null)
            {
                return false;
            }

            return true;
        }

        private RelayCommand _saveCarCommand;

        public RelayCommand SaveCarCommand
        {
            get => _saveCarCommand;
        }

        private void SaveCar()
        {
            if (!_cars.Contains(EditedCar))
            {
                _cars.Add(FormCar);
                blc.AddCar(FormCar);
            }
            else
            {
                var index = _cars.IndexOf(EditedCar);
                _cars[index] = FormCar;
                blc.UpdateCar(EditedCar, FormCar);
            }
            EditedCar = null;
        }

        private bool CanSaveCar()
        {
            if ((EditedCar != null) && (FormCar != null) && !FormCar.HasErrors)
            {
                return true;
            }

            return false;
        }

        private RelayCommand _deleteCarCommand;

        public RelayCommand DeleteCarCommand
        {
            get => _deleteCarCommand;
        }

        private void DeleteCar()
        {
            if (_cars.Contains(EditedCar))
            {
                _cars.Remove(EditedCar);
                blc.DeleteCar(EditedCar);
            }
            else
            {
                EditedCar = null;
            }
        }

        private bool CanDeleteCar()
        {
            if (EditedCar != null)
            {
                return true;
            }

            return false;
        }

        private void GetAllProducers()
        {
            foreach (var producer in blc.GetProducers())
            {
                _producers.Add(new ProducerViewModel(producer));
            }
        }

        private ProducerViewModel _formProducer;
        public ProducerViewModel FormProducer
        {
            get => _formProducer;
            set
            {
                _formProducer = value;
                OnPropertyChanged(nameof(FormProducer));
                OnPropertyChanged(nameof(FormEnabledProducer));
            }
        }

        private ProducerViewModel _editedProducer;
        public ProducerViewModel EditedProducer
        {
            get => _editedProducer;
            set
            {
                _editedProducer = value;
                if (value != null)
                {
                    FormProducer = new ProducerViewModel(blc.CreateEmptyProducer());
                    FormProducer.Name = value.Name;
                }
                else
                {
                    FormProducer = null;
                }
                OnPropertyChanged(nameof(EditedProducer));
            }
        }

        private void AddNewProducer()
        {
            EditedProducer = new ProducerViewModel(blc.CreateEmptyProducer());
            EditedProducer.Name = "";
            EditedProducer.Validate();
        }

        private RelayCommand _addNewProducerCommand;

        public RelayCommand AddNewProducerCommand
        {
            get => _addNewProducerCommand;
        }

        private bool CanAddNewProducer()
        {
            if (EditedProducer != null)
            {
                return false;
            }

            return true;
        }

        private RelayCommand _saveProducerCommand;

        public RelayCommand SaveProducerCommand
        {
            get => _saveProducerCommand;
        }

        private void SaveProducer()
        {
            Producers.Add(FormProducer);
            blc.AddProducer(FormProducer);
            EditedProducer = null;
        }

        private bool CanSaveProducer()
        {
            if ((EditedProducer != null) && (FormProducer != null) && !FormProducer.HasErrors)
            {
                return true;
            }

            return false;
        }

        private RelayCommand _cancelProducerCommand;

        public RelayCommand CancelProducerCommand
        {
            get => _cancelProducerCommand;
        }

        private void CancelProducer()
        {
            EditedProducer = null;
        }

        private bool CanCancelProducer()
        {
            if (EditedProducer != null)
            {
                return true;
            }

            return false;
        }



        public Visibility FormEnabled
        {
            get { return FormCar != null ? Visibility.Visible : Visibility.Collapsed; }
            set { }
        }

        public Visibility FormEnabledProducer
        {
            get { return FormProducer != null ? Visibility.Visible : Visibility.Collapsed; }
            set { }
        }

    }
}
