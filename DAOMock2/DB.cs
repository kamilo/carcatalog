﻿using Interfaces;
using System;
using System.Collections.Generic;

namespace DAOMock2
{
    public class DB : IDAO
    {
        private List<IProducer> _producers;
        private List<ICar> _cars;

        public DB()
        {
            _producers = new List<IProducer>()
            {
                new BO.Producent{ ID=1, Name="Honda", Founded=1992 },
                new BO.Producent{ ID=2, Name="Toyota", Founded=1966 },
                new BO.Producent{ ID=3, Name="Mercedes", Founded=1890},
                new BO.Producent{ ID=4, Name="Volkswagen", Founded=1937}
            };

            _cars = new List<ICar>()
            {
                new BO.Car
                {
                    ID =1,
                    Name ="Outback",
                    Producer = _producers[3],
                    ProductionYear =2001,
                    Transmission =Core.TransmissionType.Manual
                },
                new BO.Car
                {
                    ID =2,
                    Name ="Ackord",
                    Producer = _producers[3],
                    ProductionYear =2006,
                    Transmission =Core.TransmissionType.Automatic
                },
                new BO.Car
                {
                    ID =3,
                    Name ="CX-5",
                    Producer = _producers[3],
                    ProductionYear =1999,
                    Transmission =Core.TransmissionType.Automatic
                },
                new BO.Car
                {
                    ID =4,
                    Name ="Sentra",
                    Producer = _producers[0],
                    ProductionYear = 1995,
                    Transmission =Core.TransmissionType.Manual
                },
                new BO.Car
                {
                    ID =5,
                    Name ="Explorer",
                    Producer = _producers[2],
                    ProductionYear =2001,
                    Transmission =Core.TransmissionType.Automatic
                },
                new BO.Car
                {
                    ID =6,
                    Name ="Corolla",
                    Producer = _producers[3],
                    ProductionYear =2002,
                    Transmission =Core.TransmissionType.Automatic
                },
                new BO.Car
                {
                    ID =7,
                    Name ="Benz",
                    Producer = _producers[2],
                    ProductionYear =2010,
                    Transmission =Core.TransmissionType.Manual
                },
                new BO.Car
                {
                    ID =8,
                    Name ="Cherooke",
                    Producer = _producers[0],
                    ProductionYear =2015,
                    Transmission =Core.TransmissionType.Automatic
                },
                new BO.Car
                {
                    ID =9,
                    Name ="Rio",
                    Producer = _producers[2],
                    ProductionYear =2010,
                    Transmission =Core.TransmissionType.Automatic
                },
                new BO.Car
                {
                    ID =10,
                    Name ="Fabia",
                    Producer = _producers[3],
                    ProductionYear =2007,
                    Transmission =Core.TransmissionType.Manual
                }


            };
        }

        public ICar CreateEmptyCar()
        {
            return new BO.Car();
        }

        public IProducer CreateEmptyProducer()
        {
            return new BO.Producent();
        }

        public IProducer CloneProducer(IProducer producer)
        {
            try
            {
                return new BO.Producent
                {
                    ID = producer.ID,
                    Name = producer.Name,
                    Founded = producer.Founded
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<ICar> GetAllCars()
        {
            return _cars;
        }

        public IEnumerable<IProducer> GetAllProducers()
        {
            return _producers;
        }

        public void DeleteCar(ICar car)
        {
            _cars.Remove(car);
        }

        public void UpdateCar(ICar oldCar, ICar newCar)
        {
            if (_cars.Contains(oldCar))
            {
                _cars[_cars.IndexOf(oldCar)] = newCar;
            }
        }

        public void AddCar(ICar car)
        {
            _cars.Add(car);
        }

        public void AddProducer(IProducer producer)
        {
            _producers.Add(producer);
        }
    }
}
