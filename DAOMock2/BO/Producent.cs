﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAOMock2.BO
{
    public class Producent : IProducer
    {
        public int ID { get; set; } = -1;
        public string Name { get; set; }
        public int Founded { get; set; } = -1;

        public override string ToString()
        {
            return $"Nazwa: {Name} ({Founded})";
        }
    }
}
